function checkAuthen() {

    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {

            document.getElementById('status3').textContent = '';
            document.getElementById('status4').textContent = '';
            document.getElementById('status5').textContent = '';
            document.getElementById('status6').textContent = '';
        } else {

            document.getElementById('status3').textContent = 'Login first';
            document.getElementById('status3').style = 'color: red';
            document.getElementById('status4').textContent = 'Login first';
            document.getElementById('status4').style = 'color: red';
            document.getElementById('status5').textContent = 'Login first';
            document.getElementById('status5').style = 'color: red';
            document.getElementById('status6').textContent = 'Login first';
            document.getElementById('status6').style = 'color: red';
        }
    });
}

function clearDataList() {

    while(document.getElementById('dataList').firstChild) {
        document.getElementById('dataList').removeChild(document.getElementById('dataList').firstChild);
    }
}

function clearUploadList() {

    while(document.getElementById('uploadList').firstChild) {
        document.getElementById('uploadList').removeChild(document.getElementById('uploadList').firstChild);
    }
}

//PASTE CREATE USER HERE !!!

function createUser() {

    let email = document.getElementById('email').value;
    let password = document.getElementById('password').value;

    firebase.auth().createUserWithEmailAndPassword(email, password).then(function () {

        document.getElementById('email').value = '';
        document.getElementById('password').value = '';
        document.getElementById('status').textContent = 'Register Completed';
        document.getElementById('status').style = 'color: green';
    }).catch(function (error) {

        document.getElementById('status').textContent = 'Please Insert Email or Password';
        document.getElementById('status').style = 'color: red';
    });
}

//PASTE AUTHENTICATION HERE !!!

function login() {

    let email = document.getElementById('email2').value;
    let password = document.getElementById('password2').value;

    firebase.auth().signInWithEmailAndPassword(email, password).then(function () {

        document.getElementById('email2').value = '';
        document.getElementById('password2').value = '';
        document.getElementById('status2').textContent = 'Login Completed';
        document.getElementById('status2').style = 'color: green';
        readData();
    }).catch(function (error) {

        document.getElementById('status2').textContent = 'Login Failed';
        document.getElementById('status2').style = 'color: red';
        readData();
    });
}

function logout() {

    firebase.auth().signOut().then(function () {

        document.getElementById('status2').textContent = 'Logout Completed';
        document.getElementById('status2').style = 'color: green';

        clearDataList();
        clearUploadList();
    });
}

//PASTE INSERT DATA HERE !!!

function insertData() {

    let data = document.getElementById('data').value;

    let firebaseDatabase = firebase.database().ref('data');

    if(data == "") {

        document.getElementById('status3').textContent = 'Please Insert Data';
        document.getElementById('status3').style = 'color: red';
    } else {

        firebaseDatabase.push({
            data: data
        }).then(function() {

            document.getElementById('data').value = '';
            document.getElementById('status3').textContent = 'Insert: ' + data;
            document.getElementById('status3').style = 'color: blue';
        }).catch(function(error) {

            document.getElementById('status3').textContent = 'Login first';
            document.getElementById('status3').style = 'color: red';
        });
    }
}

//PASTE READ DATA HERE !!!

function readData() {

    checkAuthen();

    let dataList = document.getElementById('dataList');
    let uploadList = document.getElementById('uploadList');

    let firebaseDatabase = firebase.database().ref('data');
    let firebaseUpload = firebase.database().ref('file');

    firebaseDatabase.on('value', function (snapshot) {

        let key = [];
        let list = [];

        clearDataList();

        snapshot.forEach(function (childSnapshot) {
            let childKey = childSnapshot.key;
            let childData = childSnapshot.val();
            key.push(childKey);
            list.push(childData);
        });

        for(let i = 0; i < key.length; i++) {

            let row = document.createElement('tr');
            let column = document.createElement('td');
            let column2 = document.createElement('td');
            column.textContent = 'Key: ' + key[i];
            column2.textContent = 'Data: ' + list[i].data;
            dataList.appendChild(row).appendChild(column);
            dataList.appendChild(row).appendChild(column2);
        }
    });

    //PASTE READ FILE HERE !!!

    firebaseUpload.on('value', function (snapshot) {
        
        let key = [];
        let list = [];

        clearUploadList();

        snapshot.forEach(function (childSnapshot) {
            let childKey = childSnapshot.key;
            let childData = childSnapshot.val();
            key.push(childKey);
            list.push(childData);
        });

        for(let i = 0; i < key.length; i++) {

            let row = document.createElement('tr');
            let column = document.createElement('td');
            let column2 = document.createElement('td');
            let link = document.createElement('a');
            column.textContent = 'Key: ' + key[i];
            link.textContent = 'File ' + (i+1);
            link.style = 'text-decoration:none';
            link.href = list[i].file;
            link.target = '_blank';
            uploadList.appendChild(row).appendChild(column);
            uploadList.appendChild(row).appendChild(column2).appendChild(link);
        }
    });
}

//PASTE UPLOAD FILE HERE !!!

let fileButton = document.getElementById('fileButton');

fileButton.addEventListener('change', function (event) {

    let file = event.target.files[0];
    let storageRef = firebase.storage().ref();

    let metadata = {
        contentType: null
    };

    let typeFile = file.name.split('.').pop();
    let datetime = new Date();
    let filename = "( " + datetime.getHours() + ":" + datetime.getMinutes() + ":" + datetime.getSeconds() +  " )"  + "." + typeFile;


    let uploadTask = storageRef.child('images/' + filename).put(file, metadata);

    uploadTask.on('state_changed', function (snapshot) {
        
        document.getElementById('status5').textContent = 'Uploading File';
        document.getElementById('status5').style = 'color: blue';
    }, function (error) {
        
        document.getElementById('status5').textContent = 'Upload Failed';
        document.getElementById('status5').style = 'color: red';
    }, function () {
        
        let downloadURL = uploadTask.snapshot.downloadURL;

        let firebaseDatabase = firebase.database().ref('file');

        firebaseDatabase.push({
            file: downloadURL
        }).then(function() {
            document.getElementById('data').value = '';
            document.getElementById('status5').textContent = 'Upload Successful';
            document.getElementById('status5').style = 'color: green';
        }).catch(function(error) {
            document.getElementById('status3').textContent = 'Login first';
            document.getElementById('status3').style = 'color: red';
        });
    });
});