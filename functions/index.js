const functions = require('firebase-functions');

exports.dbtrig = functions.database.ref('/data/{pushId}/data').onWrite(event => {
  console.log(event.data.val());
  const original = event.data.val();
  const uppercase = original.toUpperCase();
  console.log(uppercase);
  return event.data.ref.parent.child('uppercase').set(uppercase);
});



